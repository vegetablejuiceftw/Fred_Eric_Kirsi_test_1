package kalkulaator;

public class Fractorial {
	
	public static int fractorial(int x) {
		if (x<=1) return 1;
		return x*fractorial(x-1);
	}
}
