package kalkulaator;

import java.util.Random;
import java.util.function.Function;

public class BogoSort {
	public static int[] FrodoSort(int[] N){
		Function<Integer,Integer> ran = (a)->((new Random()).nextInt(N.length - a)+a);
		for(int i=1,r1=0,r2=0;;r1=ran.apply(0),r2=ran.apply(r1),i=1){	 
	    	
	        for(;i<N.length && N[i-1]<=N[i];i++);
	        if ( i>=N.length) return N;   

	        int t1	= N[r1],t2=N[r2];
	        N[r1] 	= (t1<t2)?t1:t2;
	        N[r2] 	= (t1<t2)?t2:t1;
	    }
	}
}
