package kalkulaator;

import java.util.Iterator;

public class PositiveEvenListSum {
	
	public static int sum(int[] arr){
		
		int s = 0;
		for (int i : arr) {
			System.out.println(i);
			if (i>0 && (i%2)==0)	s+=i;
		}
		return s;
	}
}
