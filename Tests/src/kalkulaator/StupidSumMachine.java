package kalkulaator;

import java.util.ArrayList;

public class StupidSumMachine {
	
	public static int sumThat(String input){
		
		int sum = 0;
		if(input.length()==0) return sum;
		String[] array =  input.split(",|;");
			
		if (array.length>2) throw new RuntimeException("more than 2 members");
		
		for (String string : array) {
			if(string.isEmpty()) continue;
			if (string.charAt(0)=='-') throw new RuntimeException("negative");
			int i = Integer.parseInt(string);
			if (i<0) throw new RuntimeException("negative");
			sum += i;
		}
		
	
		
		return sum;
		
		
	}
}
