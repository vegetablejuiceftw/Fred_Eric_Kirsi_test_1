package kalkulaatortest;

import static org.junit.Assert.*;

import org.junit.Test;


import kalkulaator.StupidSumMachine;

public class StupidSumMachineTests {
	//CONFLICTING TESTS
	//Sisendiks 3 numbrit, tulemuseks erand ("1,2,3,4" → exception)
	//Add meetod sisendis lõpmatu arv numbreid ("1,2,3,4"→10)
	
	@Test(expected = RuntimeException.class)
	public void exceptionTest()
	{
		assertEquals("Result is different then expected",1, StupidSumMachine.sumThat("1,2,3,4,5") );
	}
	
	@Test
	public void emptyTest()
	{
		assertEquals("Result is different then expected",0, StupidSumMachine.sumThat("") );
	}
	@Test
	public void oneArgTest()
	{
		assertEquals("Result is different then expected",1, StupidSumMachine.sumThat("1") );
	}
	@Test
	public void twoArgTest()
	{
		assertEquals("Result is different then expected",3, StupidSumMachine.sumThat("1,2") );
	}
	
	@Test
	public void separator2Test()
	{
		assertEquals("Result is different then expected",3, StupidSumMachine.sumThat("1;2") );
	}
	
	@Test(expected = RuntimeException.class)
	public void negativeExceptionTest()
	{
		assertEquals("Result is different then expected",3, StupidSumMachine.sumThat("-1;2") );
	}
	
	@Test(expected = RuntimeException.class)
	public void negativeExceptionTest2()
	{
		assertEquals("Result is different then expected",3, StupidSumMachine.sumThat("-1;-2") );
	}

	@Test(expected = RuntimeException.class)
	public void negativeExceptionTest3()
	{
		assertEquals("Result is different then expected",3, StupidSumMachine.sumThat("1;-2") );
	}
	
	@Test()
	public void zeroIsAllowed()
	{
		assertEquals("Result is different then expected",0, StupidSumMachine.sumThat("0;0") );
	}
	
	@Test(expected = RuntimeException.class)
	public void negativeZeroIsNotAllowed()
	{
		assertEquals("Result is different then expected",3, StupidSumMachine.sumThat("0;-0") );
	}
	
	@Test()
	public void emptyMeansNull()
	{
		assertEquals("Result is different then expected",3, StupidSumMachine.sumThat(";3") );
	}
}
