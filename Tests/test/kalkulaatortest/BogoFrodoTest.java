package kalkulaatortest;

import static org.junit.Assert.assertEquals;

import java.util.Random;

import org.junit.Test;

import kalkulaator.BogoSort;
import kalkulaator.OnePlusOne;

public class BogoFrodoTest {
	
	public boolean inOrder(int[] arr){
		int i=1;
		for(;i<arr.length && arr[i-1]<=arr[i];i++);
        if ( i>=arr.length) return true;
        return false;
	}
	
	@Test
	public void testSimple()
	{
		int[] inp = {16,-1,2,13,14,15,0,-5};
		BogoSort.FrodoSort(inp );
		for (int i : inp) System.out.println(i);
		boolean result = inOrder(inp);
		assertEquals("Result is different then expected",true,result );
	}
	
	@Test
	public void testRandom()
	{
		int[] inp = new int[1000];
		for (int i = 0; i < 1000; i++) {
			inp[i] = (new Random()).nextInt(10000)-5000;
		}
		BogoSort.FrodoSort(inp );
//		for (int i : inp) System.out.println(i);
		boolean result = inOrder(inp);
		assertEquals("Result is different then expected",true,result );
	}
	
	@Test
	public void testEmpty()
	{
		int[] inp = {};
		BogoSort.FrodoSort(inp );
		boolean result = inOrder(inp);
		assertEquals("Result is different then expected",true,result );
	}
	
	@Test
	public void testOneElem()
	{
		int[] inp = {16};
		BogoSort.FrodoSort(inp );
//		for (int i : inp) System.out.println(i);
		boolean result = inOrder(inp);
		assertEquals("Result is different then expected",true,result );
	}
	@Test
	public void testTwoElem()
	{
		int[] inp = {16,-1};
		BogoSort.FrodoSort(inp );
//		for (int i : inp) System.out.println(i);
		boolean result = inOrder(inp);
		assertEquals("Result is different then expected",true,result );
	}
	
	@Test
	public void testTwoElemSorted()
	{
		int[] inp = {-50,100};
		BogoSort.FrodoSort(inp );
//		for (int i : inp) System.out.println(i);
		boolean result = inOrder(inp);
		assertEquals("Result is different then expected",true,result );
	}
	
	@Test
	public void testTree()
	{
		int[] inp = {-50,0,100};
		BogoSort.FrodoSort(inp );
//		for (int i : inp) System.out.println(i);
		boolean result = inOrder(inp);
		assertEquals("Result is different then expected",true,result );
	}
	
	
}
