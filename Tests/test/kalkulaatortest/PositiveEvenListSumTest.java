package kalkulaatortest;

import static org.junit.Assert.*;

import org.junit.Test;

import kalkulaator.PositiveEvenListSum;

public class PositiveEvenListSumTest
{
	
	@Test
	public void test9()
	{
		assertEquals("Result is different then expected",0, PositiveEvenListSum.sum(new int[]{-0}));
	}
	
	@Test
	public void test()
	{
		assertEquals("Result is different then expected",0, PositiveEvenListSum.sum(new int[]{1,23}));
	}
	@Test
	public void test2()
	{
		assertEquals("Result is different then expected",12, PositiveEvenListSum.sum(new int[]{2,4,6}));
	}
	@Test
	public void test3()
	{
		assertEquals("Result is different then expected",2, PositiveEvenListSum.sum(new int[]{2}));
	}
	@Test
	public void test4()
	{
		assertEquals("Result is different then expected",12, PositiveEvenListSum.sum(new int[]{-2, -4, 2, 4, 6}));
	}
	@Test
	public void test5()
	{
		assertEquals("Result is different then expected",12, PositiveEvenListSum.sum(new int[]{1, 2, 4, 6}));
	}
	@Test
	public void test6()
	{
		assertEquals("Result is different then expected",14, PositiveEvenListSum.sum(new int[]{2, -4, 1, 2, 4, 6}));
	}
	@Test
	public void test7()
	{
		assertEquals("Result is different then expected",0, PositiveEvenListSum.sum(new int[]{}));
	}
	@Test
	public void test8()
	{
		assertEquals("Result is different then expected",0, PositiveEvenListSum.sum(new int[]{-1,1,-1,1}));
	}
}
