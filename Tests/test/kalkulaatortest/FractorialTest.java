package kalkulaatortest;

import static org.junit.Assert.*;

import org.junit.Test;

import kalkulaator.Fractorial;

public class FractorialTest {

	@Test
	public void test()
	{
		assertEquals("Result is different then expected",1, Fractorial.fractorial(0) );
		
		assertEquals("Result is different then expected",1, Fractorial.fractorial(1) );
		
		assertEquals("Result is different then expected",2, Fractorial.fractorial(2) );
		
		assertEquals("Result is different then expected",6, Fractorial.fractorial(3) );
		
		assertEquals("Result is different then expected",24, Fractorial.fractorial(4) );
	}
}
